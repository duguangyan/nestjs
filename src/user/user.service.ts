import { Injectable } from '@nestjs/common';
import axios from 'axios';

@Injectable()
export class UserService {

  async getBimToken(): Promise<String> {

    let authorization = 'Basic UDdhYjNFaWtSb1RTN3g1dHhmclE5TndkR3BSeDFCc3I6cUU2Mk5nNmx1dEpaMDRBV0oyRG9QR2lkWUNmV3VJZm8='
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': authorization
    }

    const url = 'https://api.bimface.com/oauth2/token'; // 替换为实际的 API URL

    const response = await axios.post(url, {}, { headers });

    const headers1 = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + response.data.data.token
    }
    const url1 = 'https://api.bimface.com/view/token'; // 替换为实际的 API URL

    const response1 = await axios.get(url1, { params: { 'fileId': '10000820421633' }, headers: headers1 });

    return response1.data.data;
  }


  getValue(): Array<Object> {
    return [
      {
        name: '电表设备',
        'value1': '2',
        'value2': '222',
        'value3': '2222222222',
      },
      {
        name: '水表设备',
        'value1': '3',
        'value2': '333333',
        'value3': '3333333333',
      }
    ];
  }
}
