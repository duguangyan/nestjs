import { Controller, Get } from '@nestjs/common';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) { }

  @Get('/token')
  getHello(): Promise<String> {
    return this.userService.getBimToken();
  }

  @Get('/value')
  getValue(): Array<Object>{
    return this.userService.getValue();
  }
}
