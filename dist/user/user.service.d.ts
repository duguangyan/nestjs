export declare class UserService {
    getBimToken(): Promise<String>;
    getValue(): Array<Object>;
}
