import { UserService } from './user.service';
export declare class UserController {
    private readonly userService;
    constructor(userService: UserService);
    getHello(): Promise<String>;
    getValue(): Array<Object>;
}
