"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const axios_1 = require("axios");
let UserService = class UserService {
    async getBimToken() {
        let authorization = 'Basic UDdhYjNFaWtSb1RTN3g1dHhmclE5TndkR3BSeDFCc3I6cUU2Mk5nNmx1dEpaMDRBV0oyRG9QR2lkWUNmV3VJZm8=';
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': authorization
        };
        const url = 'https://api.bimface.com/oauth2/token';
        const response = await axios_1.default.post(url, {}, { headers });
        const headers1 = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + response.data.data.token
        };
        const url1 = 'https://api.bimface.com/view/token';
        const response1 = await axios_1.default.get(url1, { params: { 'fileId': '10000820421633' }, headers: headers1 });
        return response1.data.data;
    }
    getValue() {
        return [
            {
                name: '电表设备',
                'value1': '2',
                'value2': '222',
                'value3': '2222222222',
            },
            {
                name: '水表设备',
                'value1': '3',
                'value2': '333333',
                'value3': '3333333333',
            }
        ];
    }
};
exports.UserService = UserService;
exports.UserService = UserService = __decorate([
    (0, common_1.Injectable)()
], UserService);
//# sourceMappingURL=user.service.js.map